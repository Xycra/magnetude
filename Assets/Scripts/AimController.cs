﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimController : MonoBehaviour {

    // The Player.
    private Transform player;

    [SerializeField] private bool puller;
    [SerializeField] private float forceAmount = 5000f;

    // The Raycast distance.
    public float raycastMaxDistance = 10f;

    private Vector3 direction = new Vector3 (1, 0);

    // The Radious that the weapon to float away from the character when aiming.
    [SerializeField] private float radius = 2f;

    private int layer_mask;

    // This is for storing which controller is being connected.
    private string controller = "C1";

    private bool flipped = false;

    // A Toggle for if firing push pull.
    private bool firing = false;

    void Start () {
        player = transform.parent.transform;
        transform.position = player.position + (radius * direction);

        if (this.transform.parent.gameObject.name == "Player1") {
            controller = "C1";
            puller = true;
        } else {
            controller = "C2";
            puller = false;
        }

        layer_mask = LayerMask.GetMask ("Ground", "Moveable");
    }

    void FixedUpdate () {
        Vector2 aim = Vector2.zero;
        aim.x = Input.GetAxis (controller + "J2Horizontal");
        aim.y = Input.GetAxis (controller + "J2Vertical");

        direction = aim;

        direction.z = 0;

        if (direction.sqrMagnitude > 0.1f) {
            // Flips the sprite based on which way it is facing.
            if (aim.x < 0 && flipped == false) {
                flipped = true;
                this.transform.parent.gameObject.GetComponent<SpriteRenderer> ().flipX = true;
            } else if (aim.x > 0 && flipped == true) {
                flipped = false;
                this.transform.parent.gameObject.GetComponent<SpriteRenderer> ().flipX = false;
            }

            float angle = Mathf.Atan2 (aim.y, aim.x) * Mathf.Rad2Deg;
            this.transform.position = player.position + (radius * direction.normalized);
            this.transform.rotation = Quaternion.Euler (new Vector3 (0, 0, angle));
        }

        // The Vector for the Raycast.
        Vector3 temp = player.position + (radius * direction.normalized);

        if (Input.GetButtonDown (controller + "Fire1")) {
            firing = true;
        }
        if (Input.GetButtonUp (controller + "Fire1")) {
            firing = false;
        }


        // If fire is press raycast to see if there is something to push or pull.
        if (firing) {
            RaycastHit2D hit = Physics2D.Raycast (temp, transform.right, raycastMaxDistance, layer_mask);

            if (hit.collider) {
                if (puller && hit.collider.gameObject.GetComponent<blockTypes> ().isPullable ()) {
                    hit.collider.gameObject.GetComponent<Rigidbody2D> ().AddForce ((hit.collider.gameObject.transform.position - transform.position).normalized * -forceAmount);
                } 
                if (!puller && hit.collider.gameObject.GetComponent<blockTypes> ().isPushable ()) {
                    hit.collider.gameObject.GetComponent<Rigidbody2D> ().AddForce ((hit.collider.gameObject.transform.position - transform.position).normalized * forceAmount);
                }
                Debug.Log ("Hit the collidable object " + hit.collider.name);
                Vector3 right = transform.TransformDirection (Vector3.right) * 10;
                Debug.DrawRay (transform.position, right, Color.red);
            } else {
                Vector3 right = transform.TransformDirection (Vector3.right) * 10;
                Debug.DrawRay (transform.position, right, Color.red);
            }
        }
    }
}
