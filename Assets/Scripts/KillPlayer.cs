﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KillPlayer : MonoBehaviour {
    
    void OnCollisionEnter2D (Collision2D collider) {
        if (collider.gameObject.tag == "Player") {
            Debug.Log ("Kill Player");
            Destroy (collider.gameObject);

            StartCoroutine (Died ());
        }
    }

    private IEnumerator Died () {
        yield return new WaitForSeconds (3);
        SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
    }
}
