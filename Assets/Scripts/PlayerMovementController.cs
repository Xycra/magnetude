﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour {
    
    [SerializeField] private float speed = 10;
    [SerializeField] private float jumpVelocity = 10;

    private string controller = "C1";

    [SerializeField] private LayerMask playerMask;

    [SerializeField] private bool canMoveInAir = true;

    private Transform myTrans;
    private Transform tagGround;
    private Rigidbody2D myBody;

    private bool isGrounded = false;

    void Start () {
        if (this.name == "Player1") {
            controller = "C1";
        } else {
            controller = "C2";
        }

        myBody = this.GetComponent<Rigidbody2D> ();
        myTrans = this.transform;
        tagGround = this.transform.GetChild (1).transform;
    }

    void FixedUpdate () {
        isGrounded = Physics2D.Linecast (myTrans.position, tagGround.position, playerMask);

        Move (Input.GetAxisRaw (controller + "J1Horizontal"));
        if (Input.GetButtonDown (controller + "Jump")) {
            Jump ();
        }
    }

    void Move (float horizonalInput) {
        if (!canMoveInAir && !isGrounded)
            return;
 
        Vector2 moveVel = myBody.velocity;
        moveVel.x = horizonalInput * speed;
        myBody.velocity = moveVel;
    }

    public void Jump () {
        if (isGrounded) {
            myBody.velocity += jumpVelocity * Vector2.up;
        }
    }
}
