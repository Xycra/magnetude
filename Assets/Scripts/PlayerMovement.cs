﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : PhysicsObject {

    [SerializeField] private float maxSpeed = 7;
    [SerializeField] private float jumpTakeOffSpeed = 7;
    [SerializeField] private float fallFactor = 10f;
    [SerializeField] private float lowJumpFactor = 2f;
    private float startGravity;

    private string controller = "C1";

    void Start () {
        if (this.name == "Player1") {
            controller = "C1";
        } else {
            controller = "C2";
        }

        startGravity = gravityModifier;
    }

    protected override void ComputeVelocity () {
        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis (controller + "J1Horizontal");

        if (Input.GetButtonDown (controller + "Jump") && grounded) {
            velocity.y = jumpTakeOffSpeed;
        } else if (Input.GetButtonUp (controller + "Jump") && velocity.y > 0) {
            gravityModifier = lowJumpFactor;
        } else if (velocity.y < 0) {
            gravityModifier = fallFactor;
        } else {
            if (velocity.y == 0) {
                gravityModifier = startGravity;
            }
        }
        targetVelocity = move * maxSpeed;
    }
}
