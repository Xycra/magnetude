﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonTrigger : MonoBehaviour {
    [SerializeField] private GameObject targetDoor;
    [SerializeField] private GameObject secondTargetDoor;
    [SerializeField] private GameObject thirdTargetDoor;

    [SerializeField] private bool playerTriggered = false;
    [SerializeField] private bool objectTriggered = false;

    [SerializeField] private bool secondDoor = false;
    [SerializeField] private bool thirdDoor = false;

    private void OnTriggerEnter2D (Collider2D collider) {
        if (collider.tag == "Player" && playerTriggered) {
            destroyDoors ();
        }
        if (collider.tag == "Object" && objectTriggered) {
            destroyDoors ();
        }
    }

    private void destroyDoors () {
        if (targetDoor != null) {
            Destroy (targetDoor);
        }
        if (secondDoor && secondTargetDoor != null) {
            Destroy (secondTargetDoor);
        }
        if (thirdDoor && thirdTargetDoor != null) {
            Destroy (secondTargetDoor);
        }
    }
}
