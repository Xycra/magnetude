﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blockTypes : MonoBehaviour {

    [SerializeField] private bool pullable;
    [SerializeField] private bool pushable;

    public bool isPushable () {
        return pushable;
    }

    public bool isPullable () {
        return pullable;
    }
}
