﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class flagScript : MonoBehaviour {

    private bool player1On = false;
    private bool player2On = false;

    private void OnTriggerEnter2D (Collider2D collider) {
        if (collider.name == "Player1") {
            Debug.Log ("Player1 On");
            player1On = true;
        } else if (collider.name == "Player2") {
            Debug.Log ("Player2 On");
            player2On = true;
        }

        if (player1On && player2On) {
            Debug.Log ("Level Complete Start Timer");
            StartCoroutine (levelCompleteTimer ());
        }
    }

    private void OnTriggerExit2D (Collider2D collider) {
        if (collider.name == "Player1") {
            Debug.Log ("Player1 Off");
            player1On = false;
        } else if (collider.name == "Player2") {
            Debug.Log ("Player2 Off");
            player2On = false;
        }
    }

    private IEnumerator levelCompleteTimer () {
        yield return new WaitForSeconds (3);
        SceneManager.LoadScene ("LevelSelect");
    }
}
