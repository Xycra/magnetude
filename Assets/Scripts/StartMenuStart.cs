﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartMenuStart : MonoBehaviour {

    [SerializeField] private AudioSource startSound;
    private Text startText;

    private bool startedBool = false;

    // Use this for initialization
    void Start () {
        startSound = this.GetComponent<AudioSource> ();
        startText = GameObject.Find ("startText").GetComponent<Text> ();
    }
	
    // Update is called once per frame
    void Update () {
        if (Input.GetButtonDown ("Submit") && !startedBool) {
            Debug.Log ("Start Pressed");
            StartCoroutine (started ());
            startSound.Play ();
            startedBool = true;
            StartCoroutine (resize ());
        }
    }

    private IEnumerator started () {
        yield return new WaitForSeconds (1);
        SceneManager.LoadScene ("LevelSelect");
    }

    private IEnumerator resize () {
        startText.fontSize = 50;
        yield return new WaitForSeconds (0.1f);
        startText.fontSize = 45;
    }
}
