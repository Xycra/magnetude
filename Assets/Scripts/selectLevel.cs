﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class selectLevel : MonoBehaviour {

    private bool player1On = false;
    private bool player2On = false;

    private AudioSource startSound;

    private bool started = false;

    void Start () {
        startSound = GameObject.Find ("game start").GetComponent<AudioSource> ();
    }

    private void OnTriggerEnter2D (Collider2D collider) {
        if (collider.name == "Player1") {
            player1On = true;
        } else if (collider.name == "Player2") {
            player2On = true;
        }
    }

    private void OnTriggerExit2D (Collider2D collider) {
        if (collider.name == "Player1") {
            player1On = false;
        } else if (collider.name == "Player2") {
            player2On = false;
        }
    }

    void FixedUpdate () {
        if (player1On && player2On && Input.GetButtonDown ("Submit") && !started) {
            started = true;
            StartCoroutine (selectLevelTimer ());
        }

        if (Input.GetButtonDown ("Cancel")) {
            StartCoroutine (returnToTitleTimer ());
        }
    }

    private IEnumerator selectLevelTimer () {
        startSound.Play ();
        yield return new WaitForSeconds (1);
        SceneManager.LoadScene (this.name);
    }

    private IEnumerator returnToTitleTimer () {
        yield return new WaitForSeconds (1);
        SceneManager.LoadScene ("StartMenu");
    }

}
