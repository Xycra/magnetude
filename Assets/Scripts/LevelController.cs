﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour {

    private AudioSource startGame;

    private bool selectedOption = false;

    void Start () {
        startGame = this.GetComponent<AudioSource> ();
    }

    void Update () {
        if (Input.GetButtonDown ("Cancel") && !selectedOption) {
            selectedOption = true;
            StartCoroutine (returnToLevelSelect ());
        }

        if (Input.GetButtonDown ("Submit") && !selectedOption) {
            selectedOption = true;
            StartCoroutine (restartLevel ());
        }
    }

    private IEnumerator returnToLevelSelect () {
        startGame.Play ();
        yield return new WaitForSeconds (0.7f);
        SceneManager.LoadScene ("LevelSelect");
    }

    private IEnumerator restartLevel () {
        startGame.Play ();
        yield return new WaitForSeconds (0.7f);
        SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
    }
}
