﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    private Transform player1;
    private Transform player2;

    [SerializeField] private float minSizeY = 5f;
    [SerializeField] private float borderBuffer = 7f;
    [SerializeField] private float maxCameraSize = 17f;

    void Start () {
        player1 = GameObject.Find ("Player1").transform;
        player2 = GameObject.Find ("Player2").transform;
    }

    private void SetCameraPos () {
        Vector3 middle = (player1.position + player2.position) * 0.5f;

        GetComponent<Camera> ().transform.position = new Vector3 (middle.x, middle.y, GetComponent<Camera> ().transform.position.z);
    }

    private void SetCameraSize () {
        float minSizeX = minSizeY * Screen.width / Screen.height;

        float width = (Mathf.Abs (player1.position.x - player2.position.x) * 0.5f) + borderBuffer;
        float height = (Mathf.Abs (player1.position.y - player2.position.y) * 0.5f) + borderBuffer;
        float camSizeX = Mathf.Max (width, minSizeX);

        float newWidth = camSizeX * Screen.height / Screen.width;

        if (this.GetComponent<Camera> ().orthographicSize >= maxCameraSize) {
            if (newWidth >= maxCameraSize) {
                this.GetComponent<Camera> ().orthographicSize = maxCameraSize;
            } else {
                this.GetComponent<Camera> ().orthographicSize = Mathf.Max (height, newWidth, minSizeY);
            }
        } else {
            this.GetComponent<Camera> ().orthographicSize = Mathf.Max (height, newWidth, minSizeY);
        }


    }

    void Update () {
        SetCameraPos ();
        SetCameraSize ();
    }
}
